use std::{
    env::args,
    fs::File,
    io::{BufRead, BufReader, Result},
};

fn main() -> Result<()> {
    let path = args().nth(1).unwrap_or("Brewfile".into());

    let file = File::open(path)?;

    fn find_precedence(s: impl AsRef<str>) -> usize {

        const PRECEDENCE: [&str; 8] = [
            "tap \"homebrew",
            "tap",
            "brew",
            "cask_args",
            "cask \"font-",
            "cask",
            "mas",
            "whalebrew",
        ];

        PRECEDENCE
            .iter()
            .enumerate()
            .find(|&(_idx, &prefix)| s.as_ref().starts_with(prefix))
            .expect(&format!("a: {}", s.as_ref()))
            .0
    }

    let mut lines = BufReader::new(file)
        .lines()
        .map(Result::unwrap)
        .filter(|line| !line.is_empty())
        .map(|line| (find_precedence(&line), line))
        .collect::<Vec<_>>();

    lines.sort_by(|a, b| a.0.cmp(&b.0));

    let lines = lines;

    // Special Casing the first line, because it has no line before it

    println!("{}", &lines[0].1);

    // iterating from the 2nd line
    lines[1..]
        .iter()
        .enumerate()
        .for_each(|(idx, (prec, line))| {
            if *prec > lines[idx].0 {
                // idx refers to the previous line
                println!("");
            }
            println!("{}", line)
        });

    Ok(())
}
